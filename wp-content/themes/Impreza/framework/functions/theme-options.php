<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Theme Options: USOF + UpSolution extendings
 *
 * Should be included in global context.
 */

add_action( 'usof_after_save', 'us_generate_theme_options_css_file' );
function us_generate_theme_options_css_file() {

	global $usof_options;

	// Just in case the function was called separately, ex. from migrations file
	usof_load_options_once();

	if ( ! isset( $usof_options['generate_css_file'] ) OR ! $usof_options['generate_css_file'] ) {
		return;
	}

	// TODO Use WP_Filesystem instead
	$wp_upload_dir = wp_upload_dir();
	$styles_dir = wp_normalize_path( $wp_upload_dir['basedir'] . '/us-assets' );
	$styles_file = $styles_dir . '/' . US_THEMENAME . '-theme-options.css';
	global $output_styles_to_file;
	$output_styles_to_file = TRUE;

	$styles_css = us_get_template( 'templates/theme-options.css' );

	if ( ! is_dir( $styles_dir ) ) {
		wp_mkdir_p( trailingslashit( $styles_dir ) );
	}
	$handle = @fopen( $styles_file, 'w' );
	if ( $handle ) {
		if ( ! fwrite( $handle, $styles_css ) ) {
			return FALSE;
		}
		fclose( $handle );

		return TRUE;
	}

	return FALSE;
}

// Using USOF for theme options
$usof_directory = $us_template_directory . '/framework/vendor/usof';
$usof_directory_uri = $us_template_directory_uri . '/framework/vendor/usof';
$usof_version = US_THEMEVERSION;
require $us_template_directory . '/framework/vendor/usof/usof.php';
