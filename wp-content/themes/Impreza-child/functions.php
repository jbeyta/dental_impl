<?php

/* Custom functions code goes here. */

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

function arphabet_widgets_init() {

	register_sidebar( array(

		'name'          => 'Footer Map',

		'id'            => 'footer_map',

		'before_widget' => '<div class="one-half"><div class="wpb_text_column "><div class="wpb_wrapper">',

		'after_widget'  => '</div></div></div>',

		'before_title'  => '<h2 class="rounded">',

		'after_title'   => '</h2>',

	) );

	register_sidebar( array(

		'name'          => 'Footer Contact',

		'id'            => 'footer_contact',

		'before_widget' => '<div class="one-half"><div class="wpb_text_column "><div class="wpb_wrapper">',

		'after_widget'  => '</div></div></div>',

		'before_title'  => '<h2 class="rounded">',

		'after_title'   => '</h2>',

	) );

	register_sidebar( array(

		'name'          => 'Footer Contact',

		'id'            => 'footer_contact',

		'before_widget' => '<div class="one-half"><div class="wpb_text_column "><div class="wpb_wrapper">',

		'after_widget'  => '</div></div></div>',

		'before_title'  => '<h2 class="rounded">',

		'after_title'   => '</h2>',

	) );
      register_sidebar( array(
        'name' => __( 'Single Post Sidebar', 'single-post-sidebar' ),
        'id' => 'single-post-sidebar',
        'description' => __( 'This is the sidebar for single posts ', 'single-post-sidebar' ),
        'before_widget' => '<div>',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="singlePostSidebar">',
	'after_title'   => '</h2>',
    ) );
      register_sidebar( array(
        'name' => __( 'Custom Blog Sidebar', 'overview-blog-sidebar' ),
        'id' => 'overview-blog-sidebar',
        'description' => __( 'This is the sidebar for blog overview ', 'overview-blog-sidebar' ),
        'before_widget' => '<div>',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="overviewBlogSidebar">',
	'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'arphabet_widgets_init' );

add_filter("gform_notification", "change_admin_notification_email", 10, 3);

function change_admin_notification_email(  $notification, $form, $entry ){
  if($notification["name"] == "Admin Notification"){
    $notification["to"] = returnPracticeOption('doc_email');
    $notification["subject"] = 'Contact E-mail from Dental Implants ' .returnPracticeOption('seo_location');
  }

  //return altered notification object
    return $notification;
}
