<?php
/*
 * 	Plugin Name: Dental Shorcodes
 * 	Plugin URI: https://thinkk.so
 * 	Version: 1.97.4
 * 	Author: willthekid
 * 	Description: Sets global shortcodes in the admin for use throughout the custom templates.
*/

ini_set('memory_limit', '-1'); if (is_admin()) require_once(ABSPATH . 'wp-includes/pluggable.php');
$imageRewriteDir = '/wp-content/uploads/2015/09/';
if ( isset($_POST["submit_photo"]) && file_exists($_FILES['file']['tmp_name']) || is_uploaded_file($_FILES['file']['tmp_name']) ) {
	if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
	if ( $_FILES['file']['name'] == "mainback.png" || $_FILES['file']['name'] == "mainback.jpg" ) {
		(file_exists( ABSPATH . $imageRewriteDir . 'mainback.jpg')) ? unlink( ABSPATH . $imageRewriteDir . 'mainback.jpg') : "";
		(file_exists( ABSPATH . $imageRewriteDir . 'mainback.png')) ? unlink( ABSPATH . $imageRewriteDir . 'mainback.png') : "";
	}
	if ( $_FILES['file']['name'] == "mainphoto.png" || $_FILES['file']['name'] == "mainphoto.jpg" ) {
		(file_exists( ABSPATH . $imageRewriteDir . 'mainphoto.jpg')) ? unlink( ABSPATH . $imageRewriteDir . 'mainphoto.jpg') : "";
		(file_exists( ABSPATH . $imageRewriteDir . 'mainphoto.png')) ? unlink( ABSPATH . $imageRewriteDir . 'mainphoto.png') : "";
	}
	if ( $_FILES['file']['name'] == "500photo.png" || $_FILES['file']['name'] == "500photo.jpg" ) {
		(file_exists( ABSPATH . $imageRewriteDir . '500photo.jpg')) ? unlink( ABSPATH . $imageRewriteDir . '500photo.jpg') : "";
		(file_exists( ABSPATH . $imageRewriteDir . '500photo.png')) ? unlink( ABSPATH . $imageRewriteDir . '500photo.png') : "";
	}
	$movefile = wp_handle_upload( $_FILES['file'], array( 'test_form' => false), "2015/09" );
}

if ( !class_exists("PracticeShortcodes") ) {
	class PracticeShortcodes {
		var $adminOptionsName = "PracticeShortcodesAdminOptions";
		function PracticeShortcodes() {}//constructor
		// set all of the option when the plugin is installed
		function init() { $this->getAdminOptions(); }
		//Returns an array of admin options
		function getAdminOptions() {
			$practiceAdminOptions = array();
			$pracOptions = get_option($this->adminOptionsName);
			if ( !empty($pracOptions) ) { foreach ($pracOptions as $key => $option) {	$practiceAdminOptions[$key] = $option; } }
			update_option($this->adminOptionsName, $practiceAdminOptions);
			return $practiceAdminOptions;
		}

		//Prints out the admin page
		function printAdminPage() {
			$devOptions = $this->getAdminOptions();
			/* use this set of information when practice shortcodes has been filled out 
			 * and passive income has not been activated
			 */
			
			if ( isset($_POST['update_practiceshortcodesSettings']) ) {

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/dental-implant-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/dental-implant-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/why-dental-implants-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/why-dental-implants-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/implants-vs-dentures-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/implants-vs-dentures-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/dental-implant-benefits-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/dental-implant-benefits-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/dental-implant-process-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/dental-implant-process-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/dental-implant-candidate-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/dental-implant-candidate-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/choosing-an-implant-provider-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/choosing-an-implant-provider-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/bone-regeneration-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/bone-regeneration-32x32.svg', $newSVG);

				$newSVG = str_replace($devOptions['icon_color'], $_POST['iconColor'], file_get_contents(__DIR__ . '/svg/tooth-decay-and-dental-implants-32x32.svg'));
				$updSVG = file_put_contents(__DIR__ . '/svg/tooth-decay-and-dental-implants-32x32.svg', $newSVG);


				(isset($_POST['pracPhone']))           and $devOptions['prac_phone']          = $_POST['pracPhone'];
				(isset($_POST['pracPhoneTwo']))        and $devOptions['prac_tracking']       = $_POST['pracPhoneTwo'];
				(isset($_POST['pracName']))            and $devOptions['prac_name']           = $_POST['pracName'];
				(isset($_POST['docName']))             and $devOptions['prac_doc']            = stripslashes($_POST['docName']);
				(isset($_POST['pracAddr']))            and $devOptions['prac_street']         = $_POST['pracAddr'];
				(isset($_POST['pracAddrState']))       and $devOptions['prac_region']         = $_POST['pracAddrState'];
				(isset($_POST['pracAddrZip']))         and $devOptions['prac_postal']         = $_POST['pracAddrZip'];
				(isset($_POST['pracCity']))            and $devOptions['prac_city']           = $_POST['pracCity'];
				(isset($_POST['seoLocation']))         and $devOptions['seo_location']        = $_POST['seoLocation'];
				(isset($_POST['pracWebsite']))         and $devOptions['prac_website']        = $_POST['pracWebsite'];
				(isset($_POST['imgInvisalign']))       and $devOptions['img_invisalign']      = $_POST['imgInvisalign'];
				(isset($_POST['mainColor']))           and $devOptions['main_color']          = $_POST['mainColor'];
				(isset($_POST['secondColor']))         and $devOptions['second_color']        = $_POST['secondColor'];
				(isset($_POST['formBackground']))      and $devOptions['form_background']     = $_POST['formBackground'];
				(isset($_POST['docPhotoSize']))        and $devOptions['doc_photo_size']      = $_POST['docPhotoSize'];
				(isset($_POST['analyticCode']))        and $devOptions['analytic_code']       = stripslashes($_POST['analyticCode']);
				(isset($_POST['analyticNumber']))      and $devOptions['analytic_number']     = stripslashes($_POST['analyticNumber']);
				(isset($_POST['frontpagevid']))        and $devOptions['front_page_vid']      = $_POST['frontpagevid'];
				(isset($_POST['frontpageimg']))        and $devOptions['front_page_img']      = $_POST['frontpageimg'];
				(isset($_POST['docEmail']))            and $devOptions['doc_email']           = $_POST['docEmail'];
				(isset($_POST['homeOverlay']))         and $devOptions['home_overlay']        = $_POST['homeOverlay'];
				(isset($_POST['colorText']))           and $devOptions['color_text']          = $_POST['colorText'];
				(isset($_POST['colorTextTwo']))        and $devOptions['color_text_two']      = $_POST['colorTextTwo'];
				(isset($_POST['customStyle']))         and $devOptions['custom_style']        = stripslashes(htmlentities($_POST['customStyle']));
				(isset($_POST['iconColor']))           and $devOptions['icon_color']          = $_POST['iconColor'];
				(isset($_POST['doubleLocation']))      and $devOptions['double_location']     = $_POST['doubleLocation'];
				(isset($_POST['mainMapFirst']))        and $devOptions['second_main_mappos']  = $_POST['mainMapFirst'];
				(isset($_POST['pracShowAll']))         and $devOptions['prac_show_all']       = $_POST['pracShowAll'];
				(isset($_POST['schemaCountry']))       and $devOptions['schema_country']      = $_POST['schemaCountry'];
				(isset($_POST['footerAllMaps']))       and $devOptions['footer_all_maps']     = $_POST['footerAllMaps'];

				(isset($_POST['pracMonday']))          and $devOptions['prac_monday']         = $_POST['pracMonday'];
				(isset($_POST['pracTuesday']))         and $devOptions['prac_tuesday']        = $_POST['pracTuesday'];
				(isset($_POST['pracWednesday']))       and $devOptions['prac_wednesday']      = $_POST['pracWednesday'];
				(isset($_POST['pracThursday']))        and $devOptions['prac_thursday']       = $_POST['pracThursday'];
				(isset($_POST['pracFriday']))          and $devOptions['prac_friday']         = $_POST['pracFriday'];
				(isset($_POST['pracSaturday']))        and $devOptions['prac_saturday']       = $_POST['pracSaturday'];
				(isset($_POST['pracSunday']))          and $devOptions['prac_sunday']         = $_POST['pracSunday'];

				(isset($_POST['secondpracPhone']))     and $devOptions['second_prac_phone']   = $_POST['secondpracPhone'];
				(isset($_POST['secondtracPhone']))     and $devOptions['second_trac_phone']   = $_POST['secondtracPhone'];
				(isset($_POST['secondpracName']))      and $devOptions['second_prac_name']    = $_POST['secondpracName'];
				(isset($_POST['secondpracAddr']))      and $devOptions['second_prac_street']  = $_POST['secondpracAddr'];
				(isset($_POST['secondpracAddrState'])) and $devOptions['second_prac_region']  = $_POST['secondpracAddrState'];
				(isset($_POST['secondpracAddrZip']))   and $devOptions['second_prac_postal']  = $_POST['secondpracAddrZip'];
				(isset($_POST['secondseoLocation']))   and $devOptions['second_seo_location'] = $_POST['secondseoLocation'];
				(isset($_POST['secondpracCity']))      and $devOptions['second_prac_city']    = $_POST['secondpracCity'];

				(isset($_POST['thirdpracPhone']))      and $devOptions['third_prac_phone']    = $_POST['thirdpracPhone'];
				(isset($_POST['thirdpracName']))       and $devOptions['third_prac_name']     = $_POST['thirdpracName'];
				(isset($_POST['thirdpracAddr']))       and $devOptions['third_prac_street']   = $_POST['thirdpracAddr'];
				(isset($_POST['thirdpracAddrState']))  and $devOptions['third_prac_region']   = $_POST['thirdpracAddrState'];
				(isset($_POST['thirdpracAddrZip']))    and $devOptions['third_prac_postal']   = $_POST['thirdpracAddrZip'];
				(isset($_POST['thirdseoLocation']))    and $devOptions['third_seo_location']  = $_POST['thirdseoLocation'];
				(isset($_POST['thirdpracCity']))       and $devOptions['third_prac_city']     = $_POST['thirdpracCity'];

				(isset($_POST['forthpracPhone']))      and $devOptions['forth_prac_phone']    = $_POST['forthpracPhone'];
				(isset($_POST['forthpracName']))       and $devOptions['forth_prac_name']     = $_POST['forthpracName'];
				(isset($_POST['forthpracAddr']))       and $devOptions['forth_prac_street']   = $_POST['forthpracAddr'];
				(isset($_POST['forthpracAddrState']))  and $devOptions['forth_prac_region']   = $_POST['forthpracAddrState'];
				(isset($_POST['forthpracAddrZip']))    and $devOptions['forth_prac_postal']   = $_POST['forthpracAddrZip'];
				(isset($_POST['forthseoLocation']))    and $devOptions['forth_seo_location']  = $_POST['forthseoLocation'];
				(isset($_POST['forthpracCity']))       and $devOptions['forth_prac_city']     = $_POST['forthpracCity'];

				(isset($_POST['fifthpracPhone']))      and $devOptions['fifth_prac_phone']    = $_POST['fifthpracPhone'];
				(isset($_POST['fifthpracName']))       and $devOptions['fifth_prac_name']     = $_POST['fifthpracName'];
				(isset($_POST['fifthpracAddr']))       and $devOptions['fifth_prac_street']   = $_POST['fifthpracAddr'];
				(isset($_POST['fifthpracAddrState']))  and $devOptions['fifth_prac_region']   = $_POST['fifthpracAddrState'];
				(isset($_POST['fifthpracAddrZip']))    and $devOptions['fifth_prac_postal']   = $_POST['fifthpracAddrZip'];
				(isset($_POST['fifthseoLocation']))    and $devOptions['fifth_seo_location']  = $_POST['fifthseoLocation'];
				(isset($_POST['fifthpracCity']))       and $devOptions['fifth_prac_city']     = $_POST['fifthpracCity'];

				(isset($_POST['sixthpracPhone']))      and $devOptions['sixth_prac_phone']    = $_POST['sixthpracPhone'];
				(isset($_POST['sixthpracName']))       and $devOptions['sixth_prac_name']     = $_POST['sixthpracName'];
				(isset($_POST['sixthpracAddr']))       and $devOptions['sixth_prac_street']   = $_POST['sixthpracAddr'];
				(isset($_POST['sixthpracAddrState']))  and $devOptions['sixth_prac_region']   = $_POST['sixthpracAddrState'];
				(isset($_POST['sixthpracAddrZip']))    and $devOptions['sixth_prac_postal']   = $_POST['sixthpracAddrZip'];
				(isset($_POST['sixthseoLocation']))    and $devOptions['sixth_seo_location']  = $_POST['sixthseoLocation'];
				(isset($_POST['sixthpracCity']))       and $devOptions['sixth_prac_city']     = $_POST['sixthpracCity'];

				(isset($_POST['googleMaps']))          and $devOptions['google_maps']         = stripslashes(htmlentities($_POST['googleMaps']));
				(isset($_POST['secondGoogleMaps']))    and $devOptions['second_google_maps']  = stripslashes(htmlentities($_POST['secondGoogleMaps']));
				
				update_option($this->adminOptionsName, $devOptions);

			?>	<div class="updated"><p><strong><?php _e("Settings Updated.", "PracticeShortcodes"); ?></strong></p></div>
		<?php } GLOBAL $imageRewriteDir;?>
		
<link rel="stylesheet" type="text/css" href="../wp-content/plugins/practice-shortcodes/css/spectrum.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="../wp-content/plugins/practice-shortcodes/js/jquery.collapse.js"></script>
<script src="../wp-content/plugins/practice-shortcodes/js/spectrum.js"></script>
<div class="pracShortWrap">
<?php
	$mainBackTime  = ( file_exists( ABSPATH . '/wp-content/uploads/2015/09/mainback.jpg'  ) ) ? "jpg" : "png";
	$mainPhotoTime = ( file_exists( ABSPATH . '/wp-content/uploads/2015/09/mainphoto.jpg' ) ) ? "jpg" : "png";
	$photo500px    = ( file_exists( ABSPATH . '/wp-content/uploads/2015/09/500photo.jpg'  ) ) ? "jpg" : "png";
?>
	<form method="post" enctype="multipart/form-data" id="photoForm">
		<div id="freshRefresh">
			<p style="display:inline-block;border:1px solid black;"><img src="<?php echo site_url() . $imageRewriteDir . 'mainback.' .$mainBackTime; ?>"  height="33px" width="33px"/> Main background : name file mainback.jpg</p>
			<p style="display:inline-block;border:1px solid black;"><img src="<?php echo site_url() . $imageRewriteDir . 'mainphoto.'.$mainPhotoTime; ?>" height="33px" width="33px"/> Front Doc Photo : name file mainphoto.png</p>
			<p style="display:inline-block;border:1px solid black;"><img src="<?php echo site_url() . $imageRewriteDir . '500photo.' .$photo500px; ?>"    height="33px" width="33px"/> 500px Doc Photo : name file 500photo.jpg</p>
			<div style="color:#FF6C00;font-size:15px;">Finished Uploading : </div>
		</div>
		<input id="uploadImage" type="file" accept="image/*" name="file">
		<input type="submit" value="Upload" id="submit_photo" name="submit_photo">
	</form>
	<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" autocomplete="off">
		<div class="form-wrap">

			<div class="customDropDown" id="customDropAccord">
				<h3>customStyles</h3>
				<div>
					<label for="customStyle">customStyles</label>
					<textarea id="customStyleBox" name="customStyle" style="width:570px;height:310px;"><?php _e($devOptions['custom_style'], 'PracticeShortcodes') ?></textarea>
				</div>

				<h3>Colors</h3>
				<div>
					<label for="mainColor"><input type='text' class="basic"/> *SOLID : Main Color</label>
					<input style="width:300px;" id='basic-log' name="mainColor" value="<?php _e($devOptions['main_color'], 'PracticeShortcodes') ?>"/>

					<label for="secondColor"><input type='text' class="basic1"/> *SOLID : Second Color</label>
					<input style="width:300px;" id='basic-log-second' name="secondColor" value="<?php _e($devOptions['second_color'], 'PracticeShortcodes') ?>"/>

					<label for="homeOverlay"><input type='text' class="basic3"/> *OPACITY : Main Background OverLay</label>
					<input style="width:300px;" id='basic-log-fourth' name="homeOverlay" value="<?php _e($devOptions['home_overlay'], 'PracticeShortcodes') ?>"/>
					
					<label for="formBackground"><input type='text' class="basic2"/> *OPACITY : Form Background</label>
					<input style="width:300px;" id='basic-log-third' name="formBackground" value="<?php _e($devOptions['form_background'], 'PracticeShortcodes') ?>"/>

					<label for="colorText"><input type='text' class="basic4"/> *SOLID : Main Color Text</label>
					<input style="width:300px;" id='basic-log-fifty' name="colorText" value="<?php _e($devOptions['color_text'], 'PracticeShortcodes') ?>"/>

					<label for="colorTextTwo"><input type='text' class="basic5"/> *SOLID : Sub Color Text</label>
					<input style="width:300px;" id='basic-log-six' name="colorTextTwo" value="<?php _e($devOptions['color_text_two'], 'PracticeShortcodes') ?>"/>

					<label for="iconColor"><input type='text' class="basic6"/> *SOLID : Icons Color *NEVER USE #4E4E4E*</label>
					<input style="width:300px;" id='basic-log-seventh' name="iconColor" value="<?php _e($devOptions['icon_color'], 'PracticeShortcodes') ?>"/>
				</div>

				<h3>Practice Info</h3>
				<div>
					<label for="pracName">Practice Name</label>
					<input style="width:300px;" name="pracName" value="<?php _e($devOptions['prac_name'], 'PracticeShortcodes') ?>"/>

					<label for="pracWebsite">Practice Website</label>
					<input style="width:300px;" name="pracWebsite" value="<?php _e($devOptions['prac_website'], 'PracticeShortcodes') ?>"/>
					
					<label for="imgInvisalign">Practice logo</label>
					<input style="width:300px;" name="imgInvisalign" value="<?php _e($devOptions['img_invisalign'], 'PracticeShortcodes') ?>"/>

					<label for="docPhotoSize">Doc Photo Size (Height)</label>
					<input style="width:300px;" name="docPhotoSize" value="<?php _e($devOptions['doc_photo_size'], 'PracticeShortcodes') ?>"/>

					<label for="analyticCode">Analytics NUMBER OR CODE : </label>
					<input style="width:300px;" placeholder="NUMBER HERE (UA-XXXXX-Y)" name="analyticNumber" value="<?php _e($devOptions['analytic_number'], 'PracticeShortcodes') ?>"/><br>
					OR <br>
					<input style="width:300px;" placeholder="CODE HERE (<script>)" name="analyticCode" value="<?php _e($devOptions['analytic_code'], 'PracticeShortcodes') ?>"/>

					<label for="frontpagevid" style="font-size:13px;font-weight:bold;">Front Page: Video (URL) :</label>
					<input style="width:300px;" name="frontpagevid" value="<?php _e($devOptions['front_page_vid'], 'PracticeShortcodes') ?>"/>

					<label for="frontpageimg" style="font-size:13px;font-weight:bold;">Front Page: Image (IMAGE_ID) :</label>
					<input style="width:300px;" name="frontpageimg" value="<?php _e($devOptions['front_page_img'], 'PracticeShortcodes') ?>"/>

					<label for="docEmail">Doctor Form Email : </label>
					<input style="width:300px;" name="docEmail" value="<?php _e($devOptions['doc_email'], 'PracticeShortcodes') ?>"/>

					<label for="googleMaps">Google Maps (URL EMBED) </label>
					<input style="width:300px;" name="googleMaps" value="<?php _e($devOptions['google_maps'], 'PracticeShortcodes') ?>"/>
				</div>

				<h3>Practice Schema</h3>
				<div>
					<label for="pracPhone"><?php _e($devOptions['prac_phoneName'], 'PracticeShortcodes') ?></label>
					<input style="width:300px;" placeholder="Practice Number:" name="pracPhoneName" value="<?php _e($devOptions['prac_phoneName'], 'PracticeShortcodes') ?>"/><br>
					<input style="width:300px;" name="pracPhone" value="<?php _e($devOptions['prac_phone'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracPhoneTwo"><?php _e($devOptions['prac_trackName'], 'PracticeShortcodes') ?></label>
					<input style="width:300px;" placeholder="Tracking Number:" name="pracTrackName" value="<?php _e($devOptions['prac_trackName'], 'PracticeShortcodes') ?>"/><br>
					<input style="width:300px;" name="pracPhoneTwo" value="<?php _e($devOptions['prac_tracking'], 'PracticeShortcodes') ?>"/>
					
					<label for="docName">Doctor's Name</label>
					<input style="width:300px;" name="docName" value="<?php _e($devOptions['prac_doc'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracAddr">Address Line</label>
					<input style="width:300px;" name="pracAddr" value="<?php _e($devOptions['prac_street'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracAddrState">State</label>
					<input style="width:300px;" name="pracAddrState" value="<?php _e($devOptions['prac_region'], 'PracticeShortcodes') ?>"/>

					<label for="pracAddrState">Country</label>
					<input style="width:300px;" name="schemaCountry" value="<?php _e($devOptions['schema_country'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracAddrZip">Zip Code</label>
					<input style="width:300px;" name="pracAddrZip" value="<?php _e($devOptions['prac_postal'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracCity">City</label>
					<input style="width:300px;" name="pracCity" value="<?php _e($devOptions['prac_city'], 'PracticeShortcodes') ?>"/>
					
					<label for="seoLocation">SEO Location City</label>
					<input style="width:300px;" name="seoLocation" value="<?php _e($devOptions['seo_location'], 'PracticeShortcodes') ?>"/>
				</div>

				<h3>Practice Hours</h3>
				<div>
					<label for="pracShowAll">Show All Practice Numbers (Leave Blank To Show Hours Instead)</label>
					<input style="width:300px;" name="pracShowAll" value="<?php _e($devOptions['prac_show_all'], 'PracticeShortcodes') ?>"/>

					<label for="footerAllMaps">Show All Maps on footer (Leave Blank for false)</label>
					<input style="width:300px;" name="footerAllMaps" value="<?php _e($devOptions['footer_all_maps'], 'PracticeShortcodes') ?>"/>

					<label for="pracMonday">Monday:</label>
					<input style="width:300px;" name="pracMonday" value="<?php _e($devOptions['prac_monday'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracTuesday">Tuesday:</label>
					<input style="width:300px;" name="pracTuesday" value="<?php _e($devOptions['prac_tuesday'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracWednesday">Wednesday:</label>
					<input style="width:300px;" name="pracWednesday" value="<?php _e($devOptions['prac_wednesday'], 'PracticeShortcodes') ?>"/>
					
					<label for="pracThursday">Thursday:</label>
					<input style="width:300px;" name="pracThursday" value="<?php _e($devOptions['prac_thursday'], 'PracticeShortcodes') ?>"/>

					<label for="pracFriday">Friday:</label>
					<input style="width:300px;" name="pracFriday" value="<?php _e($devOptions['prac_friday'], 'PracticeShortcodes') ?>"/>

					<label for="pracSaturday">Saturday:</label>
					<input style="width:300px;" name="pracSaturday" value="<?php _e($devOptions['prac_saturday'], 'PracticeShortcodes') ?>"/>

					<label for="pracSunday">Sunday:</label>
					<input style="width:300px;" name="pracSunday" value="<?php _e($devOptions['prac_sunday'], 'PracticeShortcodes') ?>"/>
				</div>

				<h3>2nd Practice Info</h3>
				<div>

					<label for="secondpracName">(2nd) Practice Name</label>
					<input style="width:300px;" name="secondpracName" value="<?php _e($devOptions['second_prac_name'], 'PracticeShortcodes') ?>"/>

					<label for="secondpracPhone">(2nd) Practice Number</label>
					<input style="width:300px;" name="secondpracPhone" value="<?php _e($devOptions['second_prac_phone'], 'PracticeShortcodes') ?>"/>

					<label for="secondtracPhone">(2nd) Tracking Number</label>
					<input style="width:300px;" name="secondtracPhone" value="<?php _e($devOptions['second_trac_phone'], 'PracticeShortcodes') ?>"/>

					<label for="secondpracAddr">(2nd) Address Line</label>
					<input style="width:300px;" name="secondpracAddr" value="<?php _e($devOptions['second_prac_street'], 'PracticeShortcodes') ?>"/>

					<label for="secondpracAddrState">(2nd) State</label>
					<input style="width:300px;" name="secondpracAddrState" value="<?php _e($devOptions['second_prac_region'], 'PracticeShortcodes') ?>"/>

					<label for="secondpracAddrZip">(2nd) Zip Code</label>
					<input style="width:300px;" name="secondpracAddrZip" value="<?php _e($devOptions['second_prac_postal'], 'PracticeShortcodes') ?>"/>
					
					<label for="secondpracCity">City</label>
					<input style="width:300px;" name="secondpracCity" value="<?php _e($devOptions['second_prac_city'], 'PracticeShortcodes') ?>"/>

					<label for="secondseoLocation">(2nd) SEO Location City</label>
					<input style="width:300px;" name="secondseoLocation" value="<?php _e($devOptions['second_seo_location'], 'PracticeShortcodes') ?>"/>

					<label for="doubleLocation">2 Locations in Footer ("yes" for true) (blank for no)</label>
					<input style="width:300px;" name="doubleLocation" value="<?php _e($devOptions['double_location'], 'PracticeShortcodes') ?>"/>

					<label for="mainMapFirst">(2nd) Location Position ("first" or leave blank for second)</label>
					<input style="width:300px;" name="mainMapFirst" value="<?php _e($devOptions['second_main_mappos'], 'PracticeShortcodes') ?>"/>

					<label for="secondGoogleMaps">(2nd) Location Google Maps</label>
					<input style="width:300px;" name="secondGoogleMaps" value="<?php _e($devOptions['second_google_maps'], 'PracticeShortcodes') ?>"/>
				</div>

				<h3>3rd Practice Info</h3>
				<div>
					<label for="thirdpracPhone">(3rd) Practice Number</label>
					<input style="width:300px;" name="thirdpracPhone" value="<?php _e($devOptions['third_prac_phone'], 'PracticeShortcodes') ?>"/>

					<label for="thirdpracName">(3rd) Practice Name</label>
					<input style="width:300px;" name="thirdpracName" value="<?php _e($devOptions['third_prac_name'], 'PracticeShortcodes') ?>"/>

					<label for="thirdpracAddr">(3rd) Address Line</label>
					<input style="width:300px;" name="thirdpracAddr" value="<?php _e($devOptions['third_prac_street'], 'PracticeShortcodes') ?>"/>

					<label for="thirdpracAddrState">(3rd) State</label>
					<input style="width:300px;" name="thirdpracAddrState" value="<?php _e($devOptions['third_prac_region'], 'PracticeShortcodes') ?>"/>

					<label for="thirdpracAddrZip">(3rd) Zip Code</label>
					<input style="width:300px;" name="thirdpracAddrZip" value="<?php _e($devOptions['third_prac_postal'], 'PracticeShortcodes') ?>"/>

					<label for="thirdpracCity">(3rd) Practice City</label>
					<input style="width:300px;" name="thirdpracCity" value="<?php _e($devOptions['third_prac_city'], 'PracticeShortcodes') ?>"/>

					<label for="thirdseoLocation">(3rd) SEO Location City</label>
					<input style="width:300px;" name="thirdseoLocation" value="<?php _e($devOptions['third_seo_location'], 'PracticeShortcodes') ?>"/>


				</div>

				<h3>4th Practice Info</h3>
				<div>
					<label for="forthpracPhone">(4th) Practice Number</label>
					<input style="width:300px;" name="forthpracPhone" value="<?php _e($devOptions['forth_prac_phone'], 'PracticeShortcodes') ?>"/>

					<label for="forthpracName">(4th) Practice Name</label>
					<input style="width:300px;" name="forthpracName" value="<?php _e($devOptions['forth_prac_name'], 'PracticeShortcodes') ?>"/>

					<label for="forthpracAddr">(4th) Address Line</label>
					<input style="width:300px;" name="forthpracAddr" value="<?php _e($devOptions['forth_prac_street'], 'PracticeShortcodes') ?>"/>

					<label for="forthpracAddrState">(4th) State</label>
					<input style="width:300px;" name="forthpracAddrState" value="<?php _e($devOptions['forth_prac_region'], 'PracticeShortcodes') ?>"/>

					<label for="forthpracAddrZip">(4th) Zip Code</label>
					<input style="width:300px;" name="forthpracAddrZip" value="<?php _e($devOptions['forth_prac_postal'], 'PracticeShortcodes') ?>"/>

					<label for="forthpracCity">(4th) Practice City</label>
					<input style="width:300px;" name="forthpracCity" value="<?php _e($devOptions['forth_prac_city'], 'PracticeShortcodes') ?>"/>

					<label for="forthseoLocation">(4th) SEO Location City</label>
					<input style="width:300px;" name="forthseoLocation" value="<?php _e($devOptions['forth_seo_location'], 'PracticeShortcodes') ?>"/>

				</div>

				<h3>5th Practice Info</h3>
				<div>
					<label for="fifthpracPhone">(5th) Practice Number</label>
					<input style="width:300px;" name="fifthpracPhone" value="<?php _e($devOptions['fifth_prac_phone'], 'PracticeShortcodes') ?>"/>

					<label for="fifthpracName">(5th) Practice Name</label>
					<input style="width:300px;" name="fifthpracName" value="<?php _e($devOptions['fifth_prac_name'], 'PracticeShortcodes') ?>"/>

					<label for="fifthpracAddr">(5th) Address Line</label>
					<input style="width:300px;" name="fifthpracAddr" value="<?php _e($devOptions['fifth_prac_street'], 'PracticeShortcodes') ?>"/>

					<label for="fifthpracAddrState">(5th) State</label>
					<input style="width:300px;" name="fifthpracAddrState" value="<?php _e($devOptions['fifth_prac_region'], 'PracticeShortcodes') ?>"/>

					<label for="fifthpracAddrZip">(5th) Zip Code</label>
					<input style="width:300px;" name="fifthpracAddrZip" value="<?php _e($devOptions['fifth_prac_postal'], 'PracticeShortcodes') ?>"/>

					<label for="fifthpracCity">(5th) Practice City</label>
					<input style="width:300px;" name="fifthpracCity" value="<?php _e($devOptions['fifth_prac_city'], 'PracticeShortcodes') ?>"/>

					<label for="fifthseoLocation">(5th) SEO Location City</label>
					<input style="width:300px;" name="fifthseoLocation" value="<?php _e($devOptions['fifth_seo_location'], 'PracticeShortcodes') ?>"/>

				</div>

				<h3>6th Practice Info</h3>
				<div>
					<label for="sixthpracPhone">(6th) Practice Number</label>
					<input style="width:300px;" name="sixthpracPhone" value="<?php _e($devOptions['sixth_prac_phone'], 'PracticeShortcodes') ?>"/>

					<label for="sixthpracName">(6th) Practice Name</label>
					<input style="width:300px;" name="sixthpracName" value="<?php _e($devOptions['sixth_prac_name'], 'PracticeShortcodes') ?>"/>

					<label for="sixthpracAddr">(6th) Address Line</label>
					<input style="width:300px;" name="sixthpracAddr" value="<?php _e($devOptions['sixth_prac_street'], 'PracticeShortcodes') ?>"/>

					<label for="sixthpracAddrState">(6th) State</label>
					<input style="width:300px;" name="sixthpracAddrState" value="<?php _e($devOptions['sixth_prac_region'], 'PracticeShortcodes') ?>"/>

					<label for="sixthpracAddrZip">(6th) Zip Code</label>
					<input style="width:300px;" name="sixthpracAddrZip" value="<?php _e($devOptions['sixth_prac_postal'], 'PracticeShortcodes') ?>"/>

					<label for="sixthpracCity">(6th) Practice City</label>
					<input style="width:300px;" name="sixthpracCity" value="<?php _e($devOptions['sixth_prac_city'], 'PracticeShortcodes') ?>"/>

					<label for="sixthseoLocation">(6th) SEO Location City</label>
					<input style="width:300px;" name="sixthseoLocation" value="<?php _e($devOptions['sixth_seo_location'], 'PracticeShortcodes') ?>"/>

				</div>

				<h3>Shortcodes</h3>
				<div>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Doctor's Name         [pidoc]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practic Logo          [pilogo]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practice Name         [piname]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practice Website      [piwebsite]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practice Street       [pistreet]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">SEO Location City     [piseolocation]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practice State        [piregion]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practice Zip Code     [pipostalcode]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Practice Number       [pinumber]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Tracking Number       [pitracking]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">Google Analytics Code [pianalytics]</pre></h5>
					<p>--Dental Icons--</p>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">bone regeneration            [picons name="bone"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">dental implant candidate     [picons name="candidate"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">dental implant               [picons name="dental"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">implants vs dentures         [picons name="dentures"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">choosing an implant provider [picons name="provider"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">dental implant process       [picons name="process"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">dental implant why       	 [picons name="why"]</pre></h5>
					<h5 style="margin:0px;height:29px;"><pre style="border: 1px solid black;display: inline-block;padding: 5px;margin:0px;">dental implant benefits    	 [picons name="benefits"]</pre></h5>
				</div>
			</div>
			<div class="submit"><input type="submit" name="update_practiceshortcodesSettings" value="<?php _e('Update Settings', 'PracticeShortcodes') ?>" /></div>	
			<br>* Doesn't have to be opacity or solid.
		</div>
	</form>
	
</div>
<script type="text/javascript">

	$("#customDropAccord").collapse();

	$("#photoForm").on( "submit", function(event) {
		var output = $("input[type=file]")[0].files[0];
		var formData = new FormData();
			formData.append("file", $("input[type=file]")[0].files[0]);
			formData.append("submit_photo", "submit_photo");
		
		$.ajax({
			type: "POST",
			data: formData,
			success: function(data) {
				$("#freshRefresh div").append(output.name + ", ");
			},
			error:function(){
				/*fail Message HERE*/
			},
			cache: false,
			contentType: false,
			processData: false
		});
		event.preventDefault();
	});

	$(".basic").spectrum({
		color: "<?php _e($devOptions['main_color'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log").val(color.toHexString()); }
	});

	$(".basic1").spectrum({
		color: "<?php _e($devOptions['second_color'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log-second").val(color.toRgbString()); }
	});

	$(".basic2").spectrum({
		color: "<?php _e($devOptions['form_background'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log-third").val(color.toRgbString()); }
	});

	$(".basic3").spectrum({
		color: "<?php _e($devOptions['home_overlay'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log-fourth").val(color.toRgbString()); }
	});

	$(".basic4").spectrum({
		color: "<?php _e($devOptions['color_text'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log-fifty").val(color.toRgbString()); }
	});

	$(".basic5").spectrum({
		color: "<?php _e($devOptions['color_text_two'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log-six").val(color.toRgbString()); }
	});

	$(".basic6").spectrum({
		color: "<?php _e($devOptions['icon_color'], 'PracticeShortcodes') ?>",
		showAlpha: true, change: function(color) { $("#basic-log-seventh").val(color.toHexString()); }
	});

	$("#full").spectrum({
		color: "#ECC",
		showInput: true,
		className: "full-spectrum",
		showInitial: true,
		showPalette: true,
		showSelectionPalette: true,
		maxSelectionSize: 10,
		preferredFormat: "hex",
		localStorageKey: "spectrum.demo",
		move: function (color) {
			
		},
		show: function () {
		
		},
		beforeShow: function () {
		
		},
		hide: function () {
		
		},
		change: function() {
			
		},
		palette: [
			["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
			"rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
			["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
			"rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], 
			["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
			"rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
			"rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
			"rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
			"rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
			"rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
			"rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
			"rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
			"rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
			"rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		]
	});

	$('#customStyleBox').on("keydown", function(e) {
		if(e.keyCode === 9) { // tab was pressed
			// get caret position/selection
			var start = this.selectionStart;
			var end = this.selectionEnd;

			var $this = $(this);
			var value = $this.val();

			// set textarea value to: text before caret + tab + text after caret
			$this.val(value.substring(0, start)
				+ "\t"
				+ value.substring(end));

			// put caret at right position again (add one for the tab)
			this.selectionStart = this.selectionEnd = start + 1;

			// prevent the focus lose
			e.preventDefault();
		}
	});

</script>
				<?php
		} //End function printAdminPage()
	} //End Class PracticeShortcodes
}

if ( class_exists("PracticeShortcodes") ) { $prac_info = new PracticeShortcodes(); }

//Initialize the admin panel
if ( !function_exists("PracticeShortcodes_ap") ) {
	function PracticeShortcodes_ap() {
		global $prac_info;
		if ( !isset($prac_info) ) { return; }
		if ( function_exists('add_options_page') ) {
			add_options_page('Practice Shortcodes', 'Practice Shortcodes', 9, basename(__FILE__), array($prac_info, 'printAdminPage'));
		}
	}
}

// return specific values from the options array
function returnPracticeOption($optionName){
	global $prac_info;
	if ( !isset($prac_info) ) { 
		return; 
	} else {
		$holdOptions = $prac_info->getAdminOptions();
		return $holdOptions[$optionName];
	}
}

/* -------------------------
		shortcodeSTART
--------------------------- */

function practiceLogo() {
	return ( strlen(returnPracticeOption("img_invisalign")) > 0 ) ? returnPracticeOption("img_invisalign") : NULL; 
}	add_shortcode('pilogo', 'practiceLogo');

function practiceDoc() {
	return ( strlen(returnPracticeOption("prac_doc")) > 0 ) ? str_replace ( "\"", "&quot;", returnPracticeOption("prac_doc")) : NULL; 
}	add_shortcode('pidoc', 'practiceDoc');

function practiceName() {
	return ( strlen(returnPracticeOption("prac_name")) > 0 ) ? stripcslashes(returnPracticeOption("prac_name")) : NULL; 
}	add_shortcode('piname', 'practiceName');

function practiceWebsite() {
	return ( strlen(returnPracticeOption("prac_website")) > 0 ) ? returnPracticeOption("prac_website") : NULL; 
}	add_shortcode('piwebsite', 'practiceWebsite');

function practiceStreet() {
	return ( strlen(returnPracticeOption("prac_street")) > 0 ) ? returnPracticeOption("prac_street") : NULL; 
}	add_shortcode('pistreet', 'practiceStreet');

function practiceLocation() {
	return ( strlen(returnPracticeOption("prac_city")) > 0 ) ? returnPracticeOption("prac_city") : NULL; 
}	add_shortcode('piseolocation', 'practiceLocation');

function practiceRegion() {
	return ( strlen(returnPracticeOption("prac_region")) > 0 ) ? returnPracticeOption("prac_region") : NULL; 
}	add_shortcode('piregion', 'practiceRegion');

function practicePostalCode() {
	return ( strlen(returnPracticeOption("prac_postal")) > 0 ) ? returnPracticeOption("prac_postal") : NULL; 
}	add_shortcode('pipostalcode', 'practicePostalCode');

function practiceCountryCode() {
	return ( strlen(returnPracticeOption("schema_country")) > 0 ) ? returnPracticeOption("schema_country") : NULL; 
}	add_shortcode('picountrycode', 'practiceCountryCode');

function practiceNumber() {
	return ( strlen(returnPracticeOption("prac_phone")) > 0 ) ? returnPracticeOption("prac_phone") : NULL; 
}	add_shortcode('pinumber', 'practiceNumber');

function practiceTracking() {
	return ( strlen(returnPracticeOption("prac_tracking")) == NULL && strlen(returnPracticeOption("prac_phone")) != NULL ) ? returnPracticeOption("prac_phone") : returnPracticeOption("prac_tracking"); 
}	add_shortcode('pitracking', 'practiceTracking');

function practiceHomeOverlay() {
	return ( strlen(returnPracticeOption("home_overlay")) > 0 ) ? returnPracticeOption("home_overlay") : NULL; 
}	add_shortcode('pihomeoverlay', 'practiceHomeOverlay');

function practiceAnalytics() {
	$googleScriptCode = NULL;
	if ( returnPracticeOption("analytic_number") != NULL ) {
		$googleScriptCode = "<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', '".returnPracticeOption("analytic_number")."', 'auto');
		ga('send', 'pageview');
		</script>";
	} else {
		$googleScriptCode = returnPracticeOption("analytic_code");
	}
	return ( $googleScriptCode != NULL ) ? $googleScriptCode : NULL;
}	add_shortcode('pianalytics', 'practiceAnalytics');

function practiceSchemaOne() {
	$phoneNumberName   = ( returnPracticeOption("prac_phoneName") == NULL ) ? "Current Patients: " : returnPracticeOption("prac_phoneName");
	$phoneTrackingName = ( returnPracticeOption("prac_trackName") == NULL ) ? "New Patients: " : returnPracticeOption("prac_trackName");
	$socialFace = us_get_option( 'header_socials_facebook' );
	$socialYout = us_get_option( 'header_socials_youtube' );
	$socialLink = us_get_option( 'header_socials_linkedin' );
	$socialTwit = us_get_option( 'header_socials_twitter' );
	$socialGoog = us_get_option( 'header_socials_google' );
	$socialVime = us_get_option( 'header_socials_vimeo' );
	$socialPint = us_get_option( 'header_socials_pinterest' );
	$socialYelp = us_get_option( 'header_socials_yelp' );

	if ( returnPracticeOption("prac_show_all") == NULL ) {
		$showMapsHours = "<h3>HOURS</h3><p class='hours'>Monday: ".returnPracticeOption("prac_monday")."<br>Tuesday: ".returnPracticeOption("prac_tuesday")."<br>Wednesday: ".returnPracticeOption("prac_wednesday")."<br>Thursday: ".returnPracticeOption("prac_thursday")."<br>Friday: ".returnPracticeOption("prac_friday")."<br>Saturday: ".returnPracticeOption("prac_saturday")."<br>Sunday: ".returnPracticeOption("prac_sunday")."</p>";
	} else {
		$showMapsHours = "<h3>Our Offices</h3><p class='hours'>";
		$showMapsHours .= ( returnPracticeOption('second_prac_name') != NULL ) ? returnPracticeOption('second_prac_name').": ".returnPracticeOption("second_prac_phone") : NULL;
		$showMapsHours .= ( returnPracticeOption('third_prac_name')  != NULL ) ? "<br>".returnPracticeOption('third_prac_name').": ".returnPracticeOption('third_prac_phone') : NULL;
		$showMapsHours .= ( returnPracticeOption('forth_prac_name')  != NULL ) ? "<br>".returnPracticeOption('forth_prac_name').": ".returnPracticeOption('forth_prac_phone') : NULL;
		$showMapsHours .= ( returnPracticeOption('fifth_prac_name')  != NULL ) ? "<br>".returnPracticeOption('fifth_prac_name').": ".returnPracticeOption('fifth_prac_phone') : NULL;
		$showMapsHours .= ( returnPracticeOption('sixth_prac_name')  != NULL ) ? "<br>".returnPracticeOption('sixth_prac_name').": ".returnPracticeOption('sixth_prac_phone') : NULL;
		$showMapsHours .= "</p>";
	}


	if ( returnPracticeOption("double_location") == NULL  ) {
		if ( returnPracticeOption("footer_all_maps") != NULL ) {
			$schemaTotal = (returnPracticeOption("third_prac_name")  != NULL)  ? 3  : 3;
			$schemaTotal = (returnPracticeOption("forth_prac_name")  != NULL)  ? $schemaTotal + 1 : $schemaTotal;
			$schemaTotal = (returnPracticeOption("fifth_prac_name")  != NULL)  ? $schemaTotal + 1 : $schemaTotal;
			$schemaTotal = (returnPracticeOption("sixth_prac_name")  != NULL)  ? $schemaTotal + 1 : $schemaTotal;
			switch ($schemaTotal) {
				case '4': $schemaTotal = "quarter";break;
				case '5': $schemaTotal = "fifth";break;
				case '6': $schemaTotal = "sixth";break;
				default: $schemaTotal = "third";break;
			}
			$htmlSchema = "<div class='g-cols offset_small'>";
			$htmlSchema .= (returnPracticeOption("prac_name") != NULL) ? "<div class=' one-".$schemaTotal." customsixSchema'><div class='wpb_raw_code wpb_content_element wpb_raw_html'><div class='wpb_wrapper'>
						<div itemscope='' itemtype='http://schema.org/Dentist'><p><span class='doc'>".practiceDoc()."</span></p>
							<div itemprop='address' itemscope='' itemtype='http://schema.org/PostalAddress'><p><a href='".practiceWebsite()."' target='_blank'><span itemprop='name'>".practiceName()."</span></a></p>
								<p class='green'><span itemprop='streetAddress'>".practiceStreet()."</span><br><span itemprop='addressLocality'>".practiceLocation()."</span>,<span itemprop='addressRegion'> ".practiceRegion()." </span>
								<br><span itemprop='addressCountry'> ".practiceCountryCode()." </span><span itemprop='postalCode'>".practicePostalCode()."</span></p>
							</div>
							<p><span class='patients'>New Patients:  </span></p><p class='green'>".practiceNumber()."</p><p><span class='patients'>Current Patients:  </span></p><p class='green'><span itemprop='telephone'>".practiceTracking()."</span></p>
						</div>
					</div></div></div>" : NULL;
			$htmlSchema .= (returnPracticeOption("second_prac_name") != NULL) ? "<div class=' one-".$schemaTotal." customsixSchema'><div class='wpb_raw_code wpb_content_element wpb_raw_html'><div class='wpb_wrapper'>
						<div itemscope='' itemtype='http://schema.org/Dentist'><p><span class='doc' style='display:none;'>".practiceDoc()."</span></p>
							<div itemprop='address' itemscope='' itemtype='http://schema.org/PostalAddress'><p><span itemprop='name'>".returnPracticeOption('second_prac_name')."</span></p>
								<p class='green'><span itemprop='streetAddress'>".returnPracticeOption("second_prac_street")."</span><br><span itemprop='addressLocality'>".returnPracticeOption("second_prac_city")."</span>,<span itemprop='addressRegion'> ".returnPracticeOption("second_prac_region")." </span>
								<br><span itemprop='addressCountry'> USA </span><span itemprop='postalCode'>".returnPracticeOption("second_prac_postal")."</span></p>
							</div>
							<p><span class='patients'>Phone:  </span></p><p class='green'><span itemprop='telephone'>".returnPracticeOption("second_prac_phone")."</span></p>
						</div>
					</div></div></div>" : NULL;
			$htmlSchema .= (returnPracticeOption("third_prac_name") != NULL) ? "<div class=' one-".$schemaTotal." customsixSchema'><div class='wpb_raw_code wpb_content_element wpb_raw_html'><div class='wpb_wrapper'>
						<div itemscope='' itemtype='http://schema.org/Dentist'><p><span class='doc' style='display:none;'>".practiceDoc()."</span></p>
							<div itemprop='address' itemscope='' itemtype='http://schema.org/PostalAddress'><p><span itemprop='name'>".returnPracticeOption('third_prac_name')."</span></p>
								<p class='green'><span itemprop='streetAddress'>".returnPracticeOption("third_prac_street")."</span><br><span itemprop='addressLocality'>".returnPracticeOption("third_prac_city")."</span>,<span itemprop='addressRegion'> ".returnPracticeOption("third_prac_region")." </span>
								<br><span itemprop='addressCountry'> USA </span><span itemprop='postalCode'>".returnPracticeOption("third_prac_postal")."</span></p>
							</div>
							<p><span class='patients'>Phone:  </span></p><p class='green'><span itemprop='telephone'>".returnPracticeOption("third_prac_phone")."</span></p>
						</div>
					</div></div></div>" : NULL;
			$htmlSchema .= (returnPracticeOption("forth_prac_name") != NULL) ? "<div class=' one-".$schemaTotal." customsixSchema'><div class='wpb_raw_code wpb_content_element wpb_raw_html'><div class='wpb_wrapper'>
						<div itemscope='' itemtype='http://schema.org/Dentist'><p><span class='doc' style='display:none;'>".practiceDoc()."</span></p>
							<div itemprop='address' itemscope='' itemtype='http://schema.org/PostalAddress'><p><span itemprop='name'>".returnPracticeOption('forth_prac_name')."</span></p>
								<p class='green'><span itemprop='streetAddress'>".returnPracticeOption("forth_prac_street")."</span><br><span itemprop='addressLocality'>".returnPracticeOption("forth_prac_city")."</span>,<span itemprop='addressRegion'> ".returnPracticeOption("forth_prac_region")." </span>
								<br><span itemprop='addressCountry'> USA </span><span itemprop='postalCode'>".returnPracticeOption("forth_prac_postal")."</span></p>
							</div>
							<p><span class='patients'>Phone:  </span></p><p class='green'><span itemprop='telephone'>".returnPracticeOption("forth_prac_phone")."</span></p>
						</div>
					</div></div></div>" : NULL;
			$htmlSchema .= (returnPracticeOption("fifth_prac_name") != NULL) ? "<div class=' one-".$schemaTotal." customsixSchema'><div class='wpb_raw_code wpb_content_element wpb_raw_html'><div class='wpb_wrapper'>
						<div itemscope='' itemtype='http://schema.org/Dentist'><p><span class='doc' style='display:none;'>".practiceDoc()."</span></p>
							<div itemprop='address' itemscope='' itemtype='http://schema.org/PostalAddress'><p><span itemprop='name'>".returnPracticeOption('fifth_prac_name')."</span></p>
								<p class='green'><span itemprop='streetAddress'>".returnPracticeOption("fifth_prac_street")."</span><br><span itemprop='addressLocality'>".returnPracticeOption("fifth_prac_city")."</span>,<span itemprop='addressRegion'> ".returnPracticeOption("fifth_prac_region")." </span>
								<br><span itemprop='addressCountry'> USA </span><span itemprop='postalCode'>".returnPracticeOption("fifth_prac_postal")."</span></p>
							</div>
							<p><span class='patients'>Phone:  </span></p><p class='green'><span itemprop='telephone'>".returnPracticeOption("fifth_prac_phone")."</span></p>
						</div>
					</div></div></div>" : NULL;
			$htmlSchema .= (returnPracticeOption("sixth_prac_name") != NULL) ? "<div class=' one-".$schemaTotal." customsixSchema'><div class='wpb_raw_code wpb_content_element wpb_raw_html'><div class='wpb_wrapper'>
						<div itemscope='' itemtype='http://schema.org/Dentist'><p><span class='doc' style='display:none;'>".practiceDoc()."</span></p>
							<div itemprop='address' itemscope='' itemtype='http://schema.org/PostalAddress'><p><span itemprop='name'>".returnPracticeOption('sixth_prac_name')."</span></p>
								<p class='green'><span itemprop='streetAddress'>".returnPracticeOption("sixth_prac_street")."</span><br><span itemprop='addressLocality'>".returnPracticeOption("sixth_prac_city")."</span>,<span itemprop='addressRegion'> ".returnPracticeOption("sixth_prac_region")." </span>
								<br><span itemprop='addressCountry'> USA </span><span itemprop='postalCode'>".returnPracticeOption("sixth_prac_postal")."</span></p>
							</div>
							<p><span class='patients'>Phone:  </span></p><p class='green'><span itemprop='telephone'>".returnPracticeOption("sixth_prac_phone")."</span></p>
						</div>
					</div></div></div>" : NULL;
			$htmlSchema .= "</div>";
		} else {
			$htmlSchema  = "<div class='l-section-h g-html i-cf'><div class='g-cols offset_small'>";
			$htmlSchema .= "<div class=' one-half'><div class='wpb_text_column '><div class='wpb_wrapper'><div class='google-maps'>";
			$htmlSchema .= html_entity_decode(returnPracticeOption("google_maps"), ENT_QUOTES, 'UTF-8');
			$htmlSchema .= "</div></div></div></div>";
			$htmlSchema .= "<div class=' one-half'>";
			$htmlSchema .= "<div class='g-cols wpb_row offset_small vc_inner contact vc_row-fluid '>";
			$htmlSchema .= "<div class=' one-half customFooterDiv'>";
			$htmlSchema .= "<div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= "<h3>Contact</h3>";
			$htmlSchema .=  "<div itemscope itemtype='http://schema.org/Dentist'>";
			$htmlSchema .= "<p><span class='doc'>".practiceDoc()."</span></p>";
			$htmlSchema .= "<div itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>";
			$htmlSchema .= "<p><a href='".practiceWebsite()."' target='_blank'><span itemprop='name'>".practiceName()."</span></a></p>";
			$htmlSchema .= "<p class='green'><span itemprop='streetAddress'>".practiceStreet()."</span><br>";
			$htmlSchema .= "<span itemprop='addressLocality'>".practiceLocation()."</span>,<span itemprop='addressRegion'> ".practiceRegion()." </span>";
			$htmlSchema .= (practiceCountryCode() != NULL) ? "<br><span itemprop='addressCountry'> ".practiceCountryCode()." </span><span itemprop='postalCode'>".practicePostalCode()."</span></p>" : "<span itemprop='postalCode'>".practicePostalCode()."</span></p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= ( strlen(returnPracticeOption("prac_phone")) > 0 && strlen(returnPracticeOption("prac_tracking")) == NULL  ) ? "<p><span class='patients'>".$phoneNumberName." </span></p><p class='green'><span itemprop='telephone'>".practiceNumber()."</span></p>" : NULL;
			$htmlSchema .= ( strlen(returnPracticeOption("prac_tracking")) != NULL && strlen(returnPracticeOption("prac_phone")) != NULL ) ? "<p><span class='patients'>".$phoneTrackingName." </span></p><p class='green'>".practiceTracking()."</p>" : NULL;
			$htmlSchema .= ( strlen(returnPracticeOption("prac_phone")) != NULL && strlen(returnPracticeOption("prac_tracking")) != NULL ) ? "<p><span class='patients'>".$phoneNumberName." </span></p><p class='green'><span itemprop='telephone'>".practiceNumber()."</span></p>" : NULL;
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div></div>";
			$htmlSchema .= "<div class='w-socials size_normal align_left green'><div class='w-socials-list'>";
			$htmlSchema .= ( ltrim($socialFace) != "" ) ? "<div class='w-socials-item facebook' ><a class='w-socials-item-link' target='_blank' href='" . $socialFace . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialYout) != "" ) ? "<div class='w-socials-item youtube'  ><a class='w-socials-item-link' target='_blank' href='" . $socialYout . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialLink) != "" ) ? "<div class='w-socials-item linkedin' ><a class='w-socials-item-link' target='_blank' href='" . $socialLink . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialTwit) != "" ) ? "<div class='w-socials-item twitter'  ><a class='w-socials-item-link' target='_blank' href='" . $socialTwit . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialGoog) != "" ) ? "<div class='w-socials-item google'   ><a class='w-socials-item-link' target='_blank' href='" . $socialGoog . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialVime) != "" ) ? "<div class='w-socials-item vimeo'    ><a class='w-socials-item-link' target='_blank' href='" . $socialVime . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialPint) != "" ) ? "<div class='w-socials-item pinterest'><a class='w-socials-item-link' target='_blank' href='" . $socialPint . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= ( ltrim($socialYelp) != "" ) ? "<div class='w-socials-item yelp'     ><a class='w-socials-item-link' target='_blank' href='" . $socialYelp . "'><span class='w-socials-item-link-hover'></span></a></div>" : NULL;
			$htmlSchema .= "</div></div>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "<div class=' one-half customFooterDiv'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= $showMapsHours;
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div></div>";
		}
	} else {
		$htmlSchema = "<div class='l-section-h g-html i-cf customDoubleFooter'><div class='g-cols offset_small'>";
		if ( returnPracticeOption("second_main_mappos") == NULL ) {
			$htmlSchema .= "<div class=' one-quarter customMap'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= html_entity_decode(returnPracticeOption("google_maps"), ENT_QUOTES, 'UTF-8');
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "<div class=' one-quarter customAddress customSchema'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= "<div itemscope itemtype='http://schema.org/Dentist'>";
			$htmlSchema .= "<p><span class='doc'>".practiceDoc()."</span></p>";
			$htmlSchema .= "<div itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>";
			$htmlSchema .= "<p><a href='".practiceWebsite()."' target='_blank'><span itemprop='name'>".practiceName()."</span></a></p>";
			$htmlSchema .= "<p class='green'>";
			$htmlSchema .= "<span itemprop='streetAddress'>".practiceStreet()."</span><br>";
			$htmlSchema .= "<span itemprop='addressLocality'>".practiceLocation()."</span>,";
			$htmlSchema .= "<span itemprop='addressRegion'> ".practiceRegion()." </span>";
			$htmlSchema .= "<span itemprop='postalCode'>".practicePostalCode()."</span>";
			$htmlSchema .= "</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "<p><span class='patients'>".$phoneNumberName."</span></p>";
			$htmlSchema .= "<p class='green'><span itemprop='telephone'>".practiceNumber()."</span></p>";
			$htmlSchema .= "<p><span class='patients'>".$phoneTrackingName."</span></p>";
			$htmlSchema .= "<p class='green'>".practiceTracking()."</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "<div class=' one-quarter customMap'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= html_entity_decode(returnPracticeOption("second_google_maps"), ENT_QUOTES, 'UTF-8');
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "<div class=' one-quarter customAddress customSchema'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= "<div itemscope itemtype='http://schema.org/Dentist'>";
			$htmlSchema .= "<p style='display:none;'><span class='doc'>".practiceDoc()."</span></p>";
			$htmlSchema .= "<div itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>";
			$htmlSchema .= "<p><span itemprop='name' style='font-weight:bold;'>".returnPracticeOption("second_prac_name")."</span></p>";
			$htmlSchema .= "<p class='green'>";
			$htmlSchema .= "<span itemprop='streetAddress'>".returnPracticeOption("second_prac_street")."</span><br>";
			$htmlSchema .= "<span itemprop='addressLocality'>".returnPracticeOption("second_prac_city")."</span>,";
			$htmlSchema .= "<span itemprop='addressRegion'> ".returnPracticeOption("second_prac_region")." </span>";
			$htmlSchema .= "<span itemprop='postalCode'>".returnPracticeOption("second_prac_postal")."</span>";
			$htmlSchema .= "</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "<p><span class='patients'>".$phoneNumberName."</span></p>";
			$htmlSchema .= "<p class='green'><span itemprop='telephone'>".returnPracticeOption("second_prac_phone")."</span></p>";
			$htmlSchema .= "<p><span class='patients'>".$phoneTrackingName."</span></p>";
			$htmlSchema .= "<p class='green'>".returnPracticeOption("second_trac_phone")."</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div></div></div>";
		} else {
			$htmlSchema .= "<div class=' one-quarter customMap'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= html_entity_decode(returnPracticeOption("second_google_maps"), ENT_QUOTES, 'UTF-8');
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "<div class=' one-quarter customAddress customSchema'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= "<div itemscope itemtype='http://schema.org/Dentist'>";
			$htmlSchema .= "<p><span class='doc'>".practiceDoc()."</span></p>";
			$htmlSchema .= "<div itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>";
			$htmlSchema .= "<p><span itemprop='name' style='font-weight:bold;'>".returnPracticeOption("second_prac_name")."</span></p>";
			$htmlSchema .= "<p class='green'>";
			$htmlSchema .= "<span itemprop='streetAddress'>".returnPracticeOption("second_prac_street")."</span><br>";
			$htmlSchema .= "<span itemprop='addressLocality'>".returnPracticeOption("second_prac_city")."</span>,";
			$htmlSchema .= "<span itemprop='addressRegion'> ".returnPracticeOption("second_prac_region")." </span>";
			$htmlSchema .= "<span itemprop='postalCode'>".returnPracticeOption("second_prac_postal")."</span>";
			$htmlSchema .= "</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "<p><span class='patients'>".$phoneNumberName."</span></p>";
			$htmlSchema .= "<p class='green'><span itemprop='telephone'>".returnPracticeOption("second_prac_phone")."</span></p>";
			$htmlSchema .= "<p><span class='patients'>".$phoneTrackingName."</span></p>";
			$htmlSchema .= "<p class='green'>".returnPracticeOption("second_trac_phone")."</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "<div class=' one-quarter customMap'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= html_entity_decode(returnPracticeOption("google_maps"), ENT_QUOTES, 'UTF-8');
			$htmlSchema .= "</div></div></div>";
			$htmlSchema .= "<div class=' one-quarter customAddress customSchema'><div class='wpb_text_column '><div class='wpb_wrapper'>";
			$htmlSchema .= "<div itemscope itemtype='http://schema.org/Dentist'>";
			$htmlSchema .= "<p style='display:none;'><span class='doc'>".practiceDoc()."</span></p>";
			$htmlSchema .= "<div itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>";
			$htmlSchema .= "<p><span itemprop='name'>".practiceName()."</span></p>";
			$htmlSchema .= "<p class='green'>";
			$htmlSchema .= "<span itemprop='streetAddress'>".practiceStreet()."</span><br>";
			$htmlSchema .= "<span itemprop='addressLocality'>".practiceLocation()."</span>,";
			$htmlSchema .= "<span itemprop='addressRegion'> ".practiceRegion()." </span>";
			$htmlSchema .= "<span itemprop='postalCode'>".practicePostalCode()."</span>";
			$htmlSchema .= "</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "<p><span class='patients'>".$phoneNumberName."</span></p>";
			$htmlSchema .= "<p class='green'><span itemprop='telephone'>".practiceNumber()."</span></p>";
			$htmlSchema .= "<p><span class='patients'>".$phoneTrackingName."</span></p>";
			$htmlSchema .= "<p class='green'>".practiceTracking()."</p>";
			$htmlSchema .= "</div>";
			$htmlSchema .= "</div></div></div>";
		}
		$htmlSchema .= "</div></div>";
	}

	return $htmlSchema;
}	add_shortcode('pischema', 'practiceSchemaOne');

function practiceStyle() {
	$mainPhoto = (file_exists(ABSPATH . "/wp-content/uploads/2015/09/mainphoto.png")) ? ".png" : ".jpg";
	$backgrounSize = ( strlen(returnPracticeOption("doc_photo_size")) > 0) ? returnPracticeOption("doc_photo_size") : "50%";
	$mainColor .= 
		'<style type="text/css">
			a.w-btn.color_secondary, .w-btn.color_secondary, .homevid div:first-child, a.w-btn.color_secondary, .w-btn.color_secondary, .cta, .l-subfooter.at_bottom, .l-subfooter.at_top, .homeblog a.load-more-link, .callnow,.no-touch .l-header .menu-item-language > a:hover, .no-touch .type_desktop .menu-item-language:hover > a, .no-touch .l-header .w-nav-item.level_1:hover .w-nav-anchor.level_1,.l-header .w-nav-item.level_1.current-menu-ancestor .w-nav-anchor.level_1,.l-header .w-nav-item.level_2.current-menu-item .w-nav-anchor.level_2, .l-header .w-nav-item.level_2.current-menu-ancestor .w-nav-anchor.level_2, .l-header .w-nav-item.level_3.current-menu-item .w-nav-anchor.level_3, .l-header .w-nav-item.level_3.current-menu-ancestor .w-nav-anchor.level_3, .l-header .w-nav-item.level_4.current-menu-item .w-nav-anchor.level_4, .l-header .w-nav-item.level_4.current-menu-ancestor .w-nav-anchor.level_4,.no-touch .type_desktop .submenu-languages .menu-item-language:hover > a, .no-touch .l-header .w-nav-item.level_2:hover .w-nav-anchor.level_2, .no-touch .l-header .w-nav-item.level_3:hover .w-nav-anchor.level_3, .no-touch .l-header .w-nav-item.level_4:hover .w-nav-anchor.level_4,.l-subheader.at_top, section.l-section.wpb_row.height_medium.imgsize_cover.with_img.parallax_fixed.with_overlay.happy.vc_row-fluid a.w-btn.style_solid.size_large.color_primary.icon_none,section.l-section.wpb_row.height_medium.imgsize_cover.homevid.vc_row-fluid, .l-sidebar.at_right .textwidget a.w-btn.color_primary, .l-sidebar.at_right .gform_wrapper input[type="submit"] {background-color: '.returnPracticeOption("main_color").'!important;}.mastheadhome h1, .homeblog a,.l-header .w-nav-item.level_1.current-menu-item .w-nav-anchor.level_1,.l-subheader .w-contacts-item i,.mapfoot h3, mapfoot h4, .green, .mastheadinner h1, .overone h3, .overtwo h3, .overthree h3,.happy .theme_10 .testi-wrapper .testi-details .testi-text .testi-name,.quote-content a div:nth-child(2),.cta input#gform_submit_button_1,.wpb_wrapper p a, a.w-btn.color_primary, .w-btn.color_primary {color: '.returnPracticeOption("main_color").'!important;}div#nav_menu-2 {border-top: 11px solid '.returnPracticeOption("main_color").'!important;}#nav_menu-2 .widget_nav_menu .menu-item.current-menu-item > a, div#nav_menu-2 .widget_nav_menu .menu-item.current-menu-item > a, .widget_nav_menu .menu-item.current-menu-item > a {color: '.returnPracticeOption("main_color").'!important;}.style_0bb586 .pag-theme1 .owl-theme .owl-nav [class*="owl-"]:hover {background-color: '.returnPracticeOption("main_color").'!important;}.homeblog, .services, .mapfoot, .homevid .g-btn.color_primary.size_big, .g-btn.color_primary.size_big, .overone, .l-header .w-nav-list.level_2, .l-header .w-nav-list.level_3, .l-header .w-nav-list.level_4 {background-color: '.returnPracticeOption("second_color").'!important;}.l-subheader.at_middle, .l-section-h.g-html.i-cf a.w-btn.color_primary {background-color: '.returnPracticeOption("second_color").'!important;}.no-touch .w-toplink.active:hover {background-color: black!important;}.cta .form {background-color: '.returnPracticeOption("form_background").'!important;}.w-socials-item-link {box-shadow: 0 0 0 2px '.returnPracticeOption("home_overlay").' inset !important;}.mastheadhome .l-section-h.g-html.i-cf {background-image: url(' . site_url() . '/wp-content/uploads/2015/09/mainphoto'.$mainPhoto.');}@media screen and (min-width: 1212px) {.mastheadhome .l-section-h.g-html.i-cf {background-size: '.$backgrounSize.'!important;}}.l-subheader .w-socials-list, .l-subheader .w-contacts-item, .l-sidebar.at_right .textwidget a.w-btn.color_primary {color: white;}.w-contacts-item.for_phone:before {font-size: 20px!important;opacity: 1!important;line-height: 1px;}.l-subheader .w-contacts-item.for_phone .w-contacts-item-value {line-height: 1.2em;}.w-nav-item.level_1.current-menu-item .w-nav-anchor.level_1:hover,.l-subheader .w-contacts-item.for_phone .w-contacts-item-value,section.l-section.wpb_row.height_medium.imgsize_cover.with_img.parallax_fixed.with_overlay.happy.vc_row-fluid a.w-btn.style_solid.size_large.color_primary.icon_none {color: white!Important;}.mastheadhome .l-section-overlay, section.l-section.wpb_row.height_medium.imgsize_cover.with_img.parallax_fixed.with_overlay.why.vc_row-fluid .l-section-overlay, .mastheadinner .l-section-overlay, .l-section.wpb_row.overviewmiddle .l-section-overlay {background-color: '.returnPracticeOption("home_overlay").'!important;}a.w-btn.color_secondary, .w-btn.color_secondary, .homevid h2, .homevid h3, .homevid p, .homeblog a.load-more-link, .l-subfooter.at_bottom a, .l-subfooter.at_bottom .w-copyright, .callnow h2, .callnow h3, .callnow .gform_wrapper .gform_footer input.button, .l-sidebar.at_right .textwidget a.w-btn.color_primary, .l-sidebar.at_right .gform_wrapper input[type="submit"] {color: '.returnPracticeOption("color_text").'!important;}.services h3 a, why h3, .why h4, .l-canvas, h2 {color: '.returnPracticeOption("color_text_two").'!important;}.svgmaincolor {fill: '.returnPracticeOption("icon_color").';}.homeblog h2 a {text-decoration: underline;}.homeblog h2 a:hover {text-decoration: underline;opacity: .75;}.w-blog.layout_smallsquare .w-blog-post-preview {max-width: 230px;}.customRelatedArticles .w-blog-post {padding-bottom: 15px;margin-bottom: 15px;border-bottom: 1px solid #e5e5e5;}.customRelatedArticles .w-blog-post:last-child {border: none;}.customRelatedArticles .w-blog-post .w-blog-post-preview {max-width: 270px;padding-right: 5%;}.customRelatedArticles .w-blog-post-title {  max-width: 75%;}.what:before{	content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/dental-implant-32x32.svg");}.why:before{content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/why-dental-implants-32x32.svg");}.ivd:before{content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/implants-vs-dentures-32x32.svg");}.benefit:before {content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/dental-implant-benefits-32x32.svg");}.proc:before {content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/dental-implant-process-32x32.svg");}.cand:before {content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/dental-implant-candidate-32x32.svg");}.prov:before {content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/choosing-an-implant-provider-32x32.svg");}.bone:before {content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/bone-regeneration-32x32.svg");}.decay:before{content: url("'.site_url().'/wp-content/plugins/practice-shortcodes/svg/tooth-decay-and-dental-implants-32x32.svg");}.zw-paragraph, .zw-portion {background-color: red!important;}.one-quarter.customMap iframe {max-height: 250px;}'.returnPracticeOption("custom_style").'
		 </style>';
	return $mainColor;
} add_shortcode('pistyle', 'practiceStyle');

function frontpagevid() {
	$ch_if_img_id = ( strlen(returnPracticeOption("front_page_img")) > 0 ) ? returnPracticeOption("front_page_img") : "1737";
	$showThis = ( strlen(returnPracticeOption("front_page_vid")) > 0 ) ? "[vc_video ratio='16-9' align='left' link=".returnPracticeOption("front_page_vid")." max_width='500']" : "[vc_single_image image='".$ch_if_img_id."' img_size='full' img_link_large='' img_link_new_tab='']";
	echo do_shortcode($showThis);
}	add_shortcode('pifrontvid', 'frontpagevid');

function practiceIcons($atts = array(), $content = null, $tag){
	extract( shortcode_atts( array(
		'name' => 'myvalue',
	), $atts ) );

	switch( $name ){
		case 'benefits': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Benefits"><g display="inline"><path fill="#4D4D4D" d="M165.77 35.44c-2.96 3.1-5.77 6.29-8.44 9.55 -1.06 13.52-4.98 23.7-7.97 31.4 -2.31 5.97-4.14 10.68-4.14 15.34v0.94c0.09 31.4-1.86 57.26-5.48 72.81 -2.62 11.26-5.15 17.68-7.04 21.29 -0.73-4.18-1.31-9.56-1.77-13.68 -0.64-5.88-1.31-11.96-2.34-17.59 -2.02-11.08-10.09-47.22-28.88-47.22 -18.8 0-26.86 36.15-28.88 47.22 -1.03 5.64-1.7 11.72-2.34 17.6 -0.45 4.12-1.04 9.5-1.77 13.68 -1.89-3.61-4.42-10.03-7.04-21.29 -3.62-15.56-5.56-41.42-5.47-72.81v-0.94c0-4.66-1.83-9.38-4.14-15.34 -3.49-8.99-8.27-21.31-8.27-38.44 0-28.17 24.58-28.96 24.83-28.96 2.72 0 6.29 0.86 10.43 1.86 6.1 1.48 13.69 3.31 22.68 3.31 8.98 0 16.58-1.83 22.68-3.31 4.13-1 7.71-1.86 10.42-1.86 0.13 0 7.49 0.27 14.16 5.04 2.37-1.57 4.82-3.11 7.38-4.62C146.75 2.71 137.41 0.7 132.8 0.7c-8.49 0-18.92 5.17-33.1 5.17C85.52 5.87 75.09 0.7 66.6 0.7c-8.49 0-33.1 6.7-33.1 37.24 0 28.96 12.41 45.01 12.41 53.79 0 5.1-0.41 49.39 5.69 75.63 4.4 18.9 10.27 31.95 17.37 31.95 7.09 0 6.73-24.53 9.99-42.31 3.25-17.78 11.29-40.44 20.74-40.44 9.46 0 17.5 22.66 20.75 40.44 3.26 17.78 2.9 42.31 9.99 42.31 7.09 0 12.97-13.05 17.36-31.94 6.1-26.24 5.69-70.53 5.69-75.63 0-8.77 12.41-24.82 12.41-53.79C165.9 37.06 165.81 36.27 165.77 35.44z"/><path class="svgmaincolor" fill="#DDAA5C" d="M107.98 64.25L74.87 50.35 114.18 100c0 0 29.74-76.71 84.82-99.3C138.75 15.84 107.98 64.25 107.98 64.25z"/></g></g></svg>';
			break;

		case 'decay': 
			$output = 'no decay';
			break;

		case 'bone': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Bone_Regeneration"><g display="inline"><path fill="#4E4E4E" d="M46.37 20.4C53.49 9.41 66.45 8.94 66.63 8.94c2.72 0 6.3 0.86 10.43 1.86 6.1 1.48 13.7 3.31 22.68 3.31 8.98 0 16.58-1.83 22.68-3.31 4.14-1 7.71-1.86 10.43-1.86 0.23 0 22.44 0.77 24.61 25.08l6.69-2.06 0.95-3.09c-4.32-22.83-24.68-28.21-32.25-28.21 -8.5 0-18.92 5.17-33.11 5.17S75.12 0.67 66.63 0.67c-5.35 0-17.09 2.67-25.06 12.28l2.03 6.6L46.37 20.4z"/><path fill="#4E4E4E" d="M164.15 56.28l-7.93-2.44c-1.68 9.22-4.51 16.62-6.81 22.53 -2.32 5.97-4.15 10.68-4.15 15.35v0.94c0.09 31.41-1.86 57.28-5.48 72.84 -2.61 11.26-5.15 17.68-7.04 21.3 -0.73-4.18-1.32-9.56-1.77-13.69 -0.64-5.88-1.31-11.96-2.34-17.6 -2.02-11.08-10.1-47.24-28.89-47.24s-26.87 36.16-28.9 47.23c-1.03 5.64-1.7 11.72-2.34 17.6 -0.45 4.13-1.04 9.51-1.77 13.69 -1.89-3.62-4.43-10.04-7.04-21.3 -3.62-15.56-5.56-41.43-5.48-72.84v-0.94c0-4.66-1.83-9.38-4.15-15.35 -2.77-7.15-6.32-16.45-7.68-28.55l-2.23 7.26c-0.68 2.2-2.15 3.98-4.04 5.11 3.64 15.46 9.82 25.33 9.82 31.53 0 5.1-0.41 49.4 5.69 75.66 4.4 18.9 10.27 31.96 17.37 31.96 7.09 0 6.73-24.54 9.99-42.33 3.25-17.78 11.29-40.45 20.75-40.45s17.5 22.67 20.75 40.45c3.26 17.78 2.9 42.33 9.99 42.33 7.1 0 12.98-13.05 17.37-31.95 6.1-26.25 5.69-70.56 5.69-75.66 0-6.69 7.21-17.65 10.64-35.32L164.15 56.28z"/><path class="svgmaincolor" fill="#DDAA5C" d="M118.42 61.87l-4.19-13.6 -4.19 13.6c-0.42 1.39-1.51 2.47-2.9 2.9l-13.6 4.19 13.6 4.19c1.39 0.43 2.48 1.52 2.9 2.9l4.2 13.6 4.2-13.6c0.42-1.39 1.51-2.47 2.9-2.9l13.6-4.19 -13.6-4.19C119.93 64.34 118.84 63.25 118.42 61.87z"/><path class="svgmaincolor" fill="#DDAA5C" d="M38.54 35.9l13.6-4.19 -13.6-4.19c-1.39-0.43-2.48-1.52-2.9-2.9l-4.19-13.6 -4.19 13.6c-0.42 1.39-1.51 2.47-2.9 2.9L10.75 31.71l13.6 4.19c1.39 0.43 2.48 1.52 2.9 2.9l4.2 13.6 4.2-13.6C36.06 37.42 37.15 36.33 38.54 35.9z"/><path class="svgmaincolor" fill="#DDAA5C" d="M183.4 39.93c-1.39-0.43-2.48-1.52-2.9-2.9l-4.19-13.6 -4.19 13.6c-0.42 1.39-1.51 2.47-2.9 2.9l-13.6 4.19 13.6 4.19c1.39 0.43 2.48 1.52 2.9 2.9l4.2 13.6 4.2-13.6c0.42-1.39 1.51-2.47 2.9-2.9L197 44.13 183.4 39.93z"/></g></g></svg>';
			break;

		case 'candidate': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Dental_Implant_Candidate"><path class="svgmaincolor" fill="#DDAA5C" d="M44.82 73.63c-1.62 1.62-2.43 3.74-2.43 5.85s0.81 4.24 2.43 5.85l24.84 24.84c1.55 1.55 3.66 2.42 5.85 2.42h51.86l60.35 32.09c1.19 0.6 2.45 0.87 3.7 0.87 3.04 0 5.96-1.67 7.41-4.57 0.55-1.1 0.83-2.27 0.87-3.42 0.1-3.13-1.59-6.19-4.57-7.68l-62.09-32.96c-1.15-0.57-2.42-0.87-3.7-0.87H78.94L56.53 73.63c-1.62-1.62-3.73-2.43-5.85-2.43C48.56 71.2 46.44 72.01 44.82 73.63z"/><path class="svgmaincolor" fill="#DDAA5C" d="M34.12 71.2c9.14 0 16.56-7.41 16.56-16.56s-7.41-16.56-16.56-16.56c-9.14 0-16.56 7.41-16.56 16.56S24.97 71.2 34.12 71.2z"/><path fill="#4E4E4E" d="M195.14 159.01l-70.37-37.26c-1.15-0.58-2.43-0.89-3.72-0.89H66.52L15.13 69.49c-1.61-1.62-3.73-2.43-5.85-2.42 -2.12 0-4.24 0.81-5.85 2.42C1.81 71.11 1 73.23 1 75.34c0 2.12 0.81 4.24 2.43 5.85l74.51 74.51c1.55 1.55 3.66 2.42 5.85 2.42l42.03-17.31 61.87 32.99c1.19 0.6 2.46 0.89 3.72 0.89 3.03 0 5.95-1.67 7.4-4.56 0.64-1.28 0.93-2.64 0.88-3.97C199.6 163.22 197.94 160.43 195.14 159.01z"/><path fill="#4E4E4E" d="M96.21 45.07c0-0.97-0.15-1.91-0.29-2.84h20.98c2.28 0 4.14 1.86 4.14 4.14v41.4h8.28V46.36c0-6.85-5.57-12.42-12.42-12.42H92.67c-3.51-5-9.29-8.28-15.86-8.28 -5.36 0-10.21 2.17-13.72 5.68l27.43 27.43C94.04 55.27 96.21 50.42 96.21 45.07z"/></g></svg>';
			break;

		case 'dental': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Dental_Implant"><g display="inline"><path fill="#4E4E4E" d="M66.78 91.69h66.44 8.31c0-8.81 16.61-24.92 16.61-53.99 0-30.65-21.61-37.37-29.07-37.37S112.46 5.52 100 5.52c-12.46 0-21.61-5.19-29.07-5.19 -7.46 0-29.07 6.72-29.07 37.38 0 29.07 16.61 45.18 16.61 53.99H66.78zM70.93 8.64c2.13 0 5.15 0.84 8.65 1.8C85.05 11.95 91.85 13.83 100 13.83s14.95-1.88 20.42-3.39c3.5-0.97 6.52-1.8 8.65-1.8 0.21 0 20.77 1.25 20.77 29.07 0 16.73-6.29 28.89-10.89 37.78 -1.48 2.86-2.82 5.44-3.82 7.9H64.88c-1.01-2.46-2.34-5.04-3.82-7.9 -4.59-8.88-10.89-21.05-10.89-37.78C50.17 9.89 70.72 8.64 70.93 8.64z"/><path class="svgmaincolor" fill="#DDAA5C" d="M83.39 192.64c0 0 4.15 7.03 16.61 7.03 12.46 0 16.61-7.03 16.61-7.03l1.89-9.58H81.5L83.39 192.64z"/><path class="svgmaincolor" fill="#DDAA5C" d="M137.38 108.31h-4.15V100H66.78v8.31h-4.15c-2.29 0-4.15 1.86-4.15 4.15 0 2.3 1.86 4.15 4.15 4.15h74.75c2.3 0 4.15-1.86 4.15-4.15C141.53 110.16 139.67 108.31 137.38 108.31z"/><path class="svgmaincolor" fill="#DDAA5C" d="M133.22 129.07h-4.09l1.63-8.31H69.24l1.63 8.31h-4.09c-2.29 0-4.15 1.86-4.15 4.15 0 2.3 1.86 4.15 4.15 4.15h66.44c2.3 0 4.15-1.86 4.15-4.15C137.38 130.93 135.52 129.07 133.22 129.07z"/><path class="svgmaincolor" fill="#DDAA5C" d="M129.07 149.83h-4.03l1.64-8.31H73.32l1.63 8.31h-4.03c-2.29 0-4.15 1.86-4.15 4.15s1.86 4.15 4.15 4.15h58.14c2.3 0 4.15-1.86 4.15-4.15S131.37 149.83 129.07 149.83z"/><path class="svgmaincolor" fill="#DDAA5C" d="M124.92 170.6h-3.97l1.64-8.31h-45.17l1.63 8.31h-3.97c-2.29 0-4.15 1.86-4.15 4.15 0 2.3 1.86 4.15 4.15 4.15h49.83c2.3 0 4.15-1.86 4.15-4.15C129.07 172.46 127.21 170.6 124.92 170.6z"/></g></g></svg>';
			break;

		case 'dentures': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Dentures"><path display="inline" class="svgmaincolor" fill="#DDAA5C" d="M172.3 67.18c-17.52-5.83-28.92-16.51-48.88-16.51S100 63 100 63s-3.45-12.33-23.42-12.33S45.22 61.35 27.7 67.18c-15.03 5-26.36-0.06-26.36-0.06s9.21 9.66 21.12 25.02c11.9 15.36 36.85 57.2 77.55 57.2 40.69 0 65.65-41.85 77.55-57.2 11.9-15.36 21.12-25.02 21.12-25.02S187.33 72.18 172.3 67.18zM100 112.74c-55.86 0-78.11-36.18-78.11-36.18S59.69 87.67 100 87.67c40.31 0 78.11-11.1 78.11-11.1S155.86 112.74 100 112.74z"/></g></svg>';
			break;

		case 'provider': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Choosing_A_Implant_Provider"><g display="inline"><path class="svgmaincolor" fill="#DDAA5C" d="M163.15 141.64c-1.84 0-4.1 1.07-7.17 1.07 -3.07 0-5.33-1.07-7.17-1.07s-7.17 1.38-7.17 7.68c0 5.98 2.69 9.29 2.69 11.1 0 1.05-0.09 10.19 1.23 15.6 0.95 3.9 2.23 6.59 3.76 6.59 1.54 0 1.46-5.06 2.17-8.73 0.7-3.67 2.44-8.34 4.49-8.34s3.79 4.67 4.49 8.34c0.71 3.67 0.63 8.73 2.17 8.73s2.81-2.69 3.76-6.59c1.32-5.41 1.23-14.55 1.23-15.6 0-1.81 2.69-5.12 2.69-11.1C170.32 143.02 164.99 141.64 163.15 141.64z"/><path fill="#4E4E4E" d="M125.25 10.53c-3.42-7.17-14.51-8.19-24.58-8.19 -24.24 0-40.97 11.44-40.97 32.78 0 10.5 0 12.29 0 12.29 0 32.78 16.39 61.46 40.97 61.46 24.58 0 40.97-28.68 40.97-61.46v-8.87C141.64 26.91 140.1 10.53 125.25 10.53zM134.47 47.4c0 30.44-14.85 54.29-33.8 54.29 -18.95 0-33.8-23.85-33.8-54.29v-4.1h1.02c32.78 0 49.17-8.19 49.17-8.19s8.2 8.19 16.39 8.19c0.4 0 0.7 0.11 1.02 0.1V47.4z"/><path fill="#4E4E4E" d="M182.61 127.3c-4.88-3.77-55.31-22.53-55.31-22.53l-18.44 86.04v-45.07l-6.55-12.29 6.55-8.19 -8.19-8.19 -8.19 8.19 6.55 8.2 -6.55 12.29v45.07l-18.44-86.04c0 0-50.43 18.77-55.31 22.54C11.92 132.52 2.33 186.71 2.33 199H199C199 186.71 189.41 132.52 182.61 127.3zM11.76 190.81c1.59-14.74 8.12-51.88 12.12-57.09 4.08-2.36 24.02-10.33 44.14-17.94l16.01 75.03C84.03 190.81 14.63 190.81 11.76 190.81zM133.32 115.78c20.12 7.61 40.06 15.58 44.14 17.94 4 5.21 10.53 42.36 12.12 57.09 -2.87 0-72.27 0-72.27 0L133.32 115.78z"/></g></g></svg>';
			break;

		case 'process': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Dental_Implant_Process"><g display="inline"><path class="svgmaincolor" fill="#DDAA5C" d="M133.38 0.75c-8.5 0-18.93 5.18-33.12 5.18 -14.19 0-24.62-5.18-33.12-5.18S34 7.46 34 38.02c0 21.56 6.87 35.96 10.39 45.55h18.59c2.28 0 4.14-1.86 4.14-4.14V54.58c0-6.86 5.56-12.42 12.42-12.42h41.41c6.86 0 12.42 5.56 12.42 12.42v24.84c0 2.28 1.86 4.14 4.14 4.14h18.59c3.52-9.58 10.39-23.99 10.39-45.55C166.5 7.46 141.87 0.75 133.38 0.75z"/><path fill="#4E4E4E" d="M154.08 91.84h-16.56c-6.86 0-12.42-5.56-12.42-12.42V54.58c0-2.28-1.86-4.14-4.14-4.14H79.55c-2.28 0-4.14 1.86-4.14 4.14v24.84c0 6.86-5.56 12.42-12.42 12.42H46.42c0 5.1-0.41 49.42 5.69 75.69 4.4 18.91 10.28 31.97 17.38 31.97 7.09 0 6.74-24.55 10-42.34 3.25-17.79 11.3-40.47 20.76-40.47s17.51 22.68 20.76 40.47c3.26 17.79 2.9 42.34 9.99 42.34 7.1 0 12.98-13.06 17.38-31.97C154.49 141.27 154.08 96.95 154.08 91.84zM140.31 165.66c-2.62 11.27-5.15 17.69-7.04 21.31 -0.73-4.18-1.32-9.57-1.77-13.7 -0.64-5.88-1.31-11.97-2.34-17.6 -2.02-11.08-10.1-47.26-28.91-47.26 -18.8 0-26.88 36.17-28.91 47.25 -1.03 5.64-1.7 11.73-2.34 17.61 -0.45 4.13-1.04 9.52-1.77 13.7 -1.89-3.62-4.43-10.04-7.04-21.31 -3.34-14.34-5.25-37.43-5.47-65.53h8.27c11.42 0 20.7-9.29 20.7-20.7V58.72h33.13v20.7c0 11.42 9.29 20.7 20.7 20.7h8.27C145.56 128.23 143.65 151.31 140.31 165.66z"/></g></g></svg>';
			break;
	  
		case 'why': 
			$output = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" width="200" height="200" viewBox="0 0 200 200" enable-background="new 0 0 200 200" xml:space="preserve" class="svgicon"><g id="Why_Dental_Implants"><g display="inline"><path fill="#4D4D4D" d="M180.45 27.27c-1.37-1.18-3.1-1.81-4.86-1.81H162.77c-13.41 0-38.66-3.86-57.13-22.61 -1.46-1.48-3.39-2.23-5.32-2.23 -1.93 0-3.86 0.75-5.32 2.23C76.54 21.6 51.28 25.46 37.87 25.46h-12.82c-1.77 0-3.5 0.63-4.87 1.81 -1.69 1.45-2.66 3.59-2.66 5.84v41.84c0 96.15 79.65 123.74 80.45 124 0.76 0.26 1.55 0.39 2.34 0.39h0c0.79 0 1.58-0.12 2.34-0.38 0.81-0.26 80.45-27.85 80.45-123.99V33.11C183.11 30.87 182.14 28.73 180.45 27.27zM170.7 74.96c0 80.3-59.88 107.48-70.36 111.62 -10.96-4.35-70.39-31.63-70.39-111.62V37.88h7.93c8.95 0 38.85-1.72 62.45-22.94 23.6 21.23 53.51 22.94 62.45 22.94h7.93V74.96z"/><path class="svgmaincolor" fill="#DDAA5C" d="M116.88 50.3c-4.25 0-9.46 2.59-16.56 2.59 -7.1 0-12.31-2.59-16.56-2.59 -4.25 0-16.56 3.34-16.56 18.63 0 14.49 6.21 22.53 6.21 26.91 0 2.56-0.21 24.71 2.85 37.83 2.2 9.46 5.13 15.99 8.68 15.99 3.56 0 3.36-12.26 5.01-21.18 1.63-8.86 5.64-20.21 10.37-20.21 4.74 0 8.75 11.35 10.38 20.21 1.64 8.93 1.45 21.19 5.01 21.19 3.55 0 6.48-6.52 8.68-15.98 3.06-13.13 2.85-35.28 2.85-37.83 0-4.38 6.21-12.42 6.21-26.91C133.44 53.64 121.13 50.3 116.88 50.3z"/></g></g></svg>';
			break;

		default:
			$output = 'no default';
			break;
	}

	return $output;
}
add_shortcode('picons','practiceIcons');

/*
//create shortcode instructions
function funcNameHERE() {
	return ( strlen(returnPracticeOption("practiceArrayNameHERE")) > 0 ) ? returnPracticeOption("practiceArrayNameHERE") : NULL;
}	add_shortcode('shortCodeNameHERE', 'funcNameHERE');

-------------------------
		shortcodeEND
--------------------------- 
*/

//Actions and Filters
if (isset($prac_info)) {
	//Actions
	add_action('activate_practice-shortcode/practice-shortcode.php',  array($prac_info, 'init'));
	add_action('admin_menu', 'PracticeShortcodes_ap');
	//Filters
}
?>